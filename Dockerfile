FROM freeradius/freeradius-server:latest
ENV GUESTVLAN 100

COPY freeradius /etc/freeradius
COPY start.sh /usr/bin/start.sh
RUN sed -i "s/Tunnel-Private-Group-Id =.*$/Tunnel-Private-Group-Id = ${GUESTVLAN}/g" /etc/freeradius/mods-config/files/authorize
#RUN chmod 777 /etc/freeradius/certs/bootstrap; /etc/freeradius/certs/bootstrap
RUN chown freerad:freerad -R /etc/freeradius; usermod -a -G ssl-cert freerad
ENTRYPOINT ["/usr/bin/start.sh"]
